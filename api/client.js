import axios from "axios";
import NetInfo from "@react-native-community/netinfo";

const apiUrl = 'https://bitbucket.org/Vinfinity_alex/hochuhavat/raw/'

export function me(token) {
    return callApi('https://bitbucket.org/Vinfinity_alex/hochuhavat/raw/4755e24b3462a86d00967ecc3188b42293b7228d/hochuhavat_backend/user/me.json', token)
}

/**
 * Resource
 */

export function getUser(username) {
    return callApi(`https://bitbucket.org/Vinfinity_alex/hochuhavat/raw/4755e24b3462a86d00967ecc3188b42293b7228d/hochuhavat_backend/user${userId}`, token)
}

export async function getProducts() {
    try {
        const url = "https://bitbucket.org/Vinfinity_alex/hochuhavat/raw/4755e24b3462a86d00967ecc3188b42293b7228d/hochuhavat_backend/product.json";
        const {data: {products}} = await axios.get(url);
        console.log(products);
        return products;
    } catch (e) {
        return "" + e;
    }
}

export function isConnected() {
    NetInfo.isConnected.fetch().then(isConnected => {
        console.log('First, is ' + (isConnected ? 'online' : 'offline'));
        return isConnected;
    });

    function handleFirstConnectivityChange(isConnected) {
        console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            handleFirstConnectivityChange
        );
    }

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        handleFirstConnectivityChange
    );
}