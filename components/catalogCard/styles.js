import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    mainContainer: {
        margin: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomColor: AppColors.Gainsboro,
        borderBottomWidth: 2,
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
    },
    mainContainerSelected: {
        margin: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomColor: AppColors.Gainsboro,
        borderBottomWidth: 2,
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        opacity: 0.1
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        color: AppColors.Black,
    },

    description: {
        fontSize: 14,
        color: AppColors.Black,
        paddingVertical: 5,
    },

    price: {
        fontSize: 16,
        color: AppColors.Black,
        paddingVertical: 5,
        fontStyle: "italic"
    },
    smallIcon: {
        height: 100,
        width: 100,
        margin: 5,
    },
    textContainer: {
        marginLeft: 10
    },
    actionsContainer:{
        flexDirection:"column",
        flex:1,
        alignItems: "center",
        justifyContent: "space-around",
    },
    detailButtonContainer: {
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10,
    },
    detailButton: {
        flex:1,
        alignItems: "center",
        justifyContent: "center",
        width: 60,
    },
    detailButtonText: {
        alignItems: "center",
        justifyContent: "center",
        margin: 10,
        color: AppColors.White,
        fontSize: 14,
    }
});

export default Styles;