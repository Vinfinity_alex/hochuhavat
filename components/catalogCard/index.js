import React from "react";
import {Text, Image, View, TouchableOpacity} from "react-native"
import Styles from "./styles";
import Icon from 'react-native-vector-icons/MaterialIcons';
import AppColors from "../../modules/appColors";
import ImageCard from "../imageCard";

const CatalogCard = props => {
    const {imagesUri, navigation, onLongPress, onPress} = props;
    return (<ImageCard imagesUri={imagesUri} navigation={navigation} onLongPress={()=>{onLongPress}} onPress={()=>{onPress}}>
        <View style={Styles.textContainer}>
            <Text style={Styles.title}>{props.title}</Text>
            <Text style={Styles.price}>Price: {props.price}</Text>
            <Text style={Styles.shortDescription}>Description: {props.shortDescription}</Text>
        </View>
        <View style={Styles.actionsContainer}>
            {/* !!TODO: Move it to separate button */}
            <TouchableOpacity style={Styles.detailButton} onPress={() => {
                navigation.push("ProductDetails", {
                    title: props.title,
                    productID: props.productID,
                    photos: imagesUri,
                    productName:props.title,
                    price: props.price,
                    shortDescription: props.shortDescription,
                    description: props.description,
                });
            }}>
                <Icon name="navigate-next" size={40} color={AppColors.Black}/>
            </TouchableOpacity>
        </View>
    </ImageCard>);
};

// !!TODO: PropTypes + default values


export default CatalogCard;
