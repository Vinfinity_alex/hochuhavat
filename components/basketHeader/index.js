import React, {Component} from "react";
import {connect} from 'react-redux';

import {Text} from "react-native";
import Styles from "./styles";
import BackButton from "../backButton";
import MainHeader from "../header";
import {totalPriceSelector} from "../../modules/selectors/basketSelectors"

class BasketHeader extends Component {
    render() {
        const{navigation, basket} = this.props;
        return (
            <MainHeader title={"Basket"} navigation={navigation} headerLeft={<BackButton navigation={navigation}/>}>
                <Text style={Styles.price} children={totalPriceSelector(basket) > 0 ? "Total: "+ totalPriceSelector(basket) + " UAH" : ""}/>
            </MainHeader>
        );
    }
}

const mapStateToProps = (state) => {
    const {basket} = state;
    return {basket}
};

export default connect(mapStateToProps)(BasketHeader);