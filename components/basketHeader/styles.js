import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    price: {
        paddingLeft:20,
        fontSize: 20,
        fontWeight: "bold",
        alignItems: "center",
        color: AppColors.White,
    },
});

export default Styles;