import React from "react";
import {ScrollView, Text, View} from "react-native";
import Styles from "./styles";

const ProductDescription = props => {
    const {price, shortDescription, description} = props;
    return (<ScrollView style={Styles.description}>
        <View style={Styles.titleBox}>
            <Text style={Styles.subtitle} children={"Price: "}/>
        </View>
        <Text style={Styles.text}>{price}<Text style={Styles.text} children={" UAH"}/></Text>
        <View style={Styles.titleBox}>
            <Text style={Styles.subtitle} children={"Short description: "}/>
        </View>
        <Text style={Styles.text} children={shortDescription}/>
        <View style={Styles.titleBox}>
            <Text style={Styles.subtitle} children={"Description: "}/>
        </View>
        <Text style={Styles.text} children={description}/>
    </ScrollView>);
};

// !!TODO: PropTypes + default values

export default ProductDescription;
