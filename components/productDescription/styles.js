import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: "bold",
        color: AppColors.Black,
        alignSelf: "stretch",
        marginBottom: 5,
        backgroundColor: AppColors.LightGrey,
    },
    subtitle: {
        fontSize: 18,
        fontWeight: "bold",
        color: AppColors.Violet,
        alignSelf: "stretch",
        marginLeft: 10,
    },
    text: {
        fontSize: 16,
        color: AppColors.Black,
        alignSelf: "stretch",
        marginHorizontal: 15,
        marginVertical: 10,
        flexWrap: 'wrap',
    },
    titleBox: {
        backgroundColor: AppColors.LightGrey,
    },
});

export default Styles;