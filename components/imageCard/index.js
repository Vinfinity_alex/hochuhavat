import React from "react";
import {Text, Image, View, TouchableOpacity} from "react-native"
import Styles from "./styles";
import Icon from 'react-native-vector-icons/MaterialIcons';
import AppColors from "../../modules/appColors";

const ImageCard = props => {
    const {imagesUri, navigation, onLongPress, onPress, children} = props;
    // !!TODO: how does it work?  onLongPress={() => {onLongPress}} onPress={() => {onPress}}
    return (<TouchableOpacity onLongPress={() => {onLongPress}} onPress={() => {onPress}}>
        <View style={Styles.mainContainer}>
            <Image style={Styles.smallIcon} source={{uri: imagesUri[0]}}/>
            {children}
        </View>
    </TouchableOpacity>);
};

// !!TODO: PropTypes + default values

export default ImageCard;
