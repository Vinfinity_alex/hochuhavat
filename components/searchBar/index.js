import React, {Component} from "react";
import {Text, Image, TextInput, View, TouchableOpacity, Animated} from "react-native"
import Styles from "./styles";
import Icon from "react-native-vector-icons/MaterialIcons";
import AppColors from "../../modules/appColors";
import BaseButton from "../button";


const SearchBar = ({style, onPress, children}) => {
    const Style = style ?? Styles.container;

    return (
        <View style={Style}>
            <TextInput style={Styles.textInput} maxLength={500} placeholder={"Search something for you"}/>
            <BaseButton onPress={onPress}><Icon style={{padding: 5}} name="search" size={30} color={AppColors.Black}/></BaseButton>
        </View>
    );
};

// !!TODO: PropTypes + default values


export default SearchBar;
