import React from "react";
import {Dimensions, StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    container: {
        backgroundColor: AppColors.White,
        alignItems: "center",
        flexDirection: "row",
        height: 40,
        borderRadius: 30,
        margin: 2,
        justifyContent: "space-between",
    },
    textInput: {
        alignSelf:"center",
        width: Dimensions.get('window').width-100.0,
    }
});

export default Styles;