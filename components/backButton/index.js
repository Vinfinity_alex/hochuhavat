import React from "react";
import Styles from "./styles";
import Icon from "react-native-vector-icons/MaterialIcons";
import AppColors from "../../modules/appColors";
import {TouchableOpacity} from "react-native";

const BackButton = ({navigation}) => {

    return (
        <TouchableOpacity style={Styles.default} onPress={() => navigation.pop()}>
            <Icon name="navigate-before" size={30} color={AppColors.White}/>
        </TouchableOpacity>
    );
};

// !!TODO: PropTypes + default values

export default BackButton;
