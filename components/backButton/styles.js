import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    default : {
        "alignItems": "center",
        padding: 10
    }
});

export default Styles;