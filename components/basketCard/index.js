import React from "react";
import {Text, Picker,  View, TouchableOpacity} from "react-native"
import Styles from "./styles";
import Icon from 'react-native-vector-icons/MaterialIcons';
import AppColors from "../../modules/appColors";
import ImageCard from "../imageCard";
import Counter from "../counter";

const BasketCard = props => {
    const {imagesUri, amount, navigation, onLongPress, onPress, onDelete, onPlus, onMinus} = props;
    return (
    <ImageCard imagesUri={imagesUri} navigation={navigation} onLongPress={()=>{onLongPress}} onPress={()=>{onPress}}>
        <View style={Styles.textContainer}>
            <Text style={Styles.title}>{props.title}</Text>
            <Text style={Styles.price}>Price: {props.price}</Text>
            <Counter onPlus={onPlus} onMinus={onMinus} count={amount}/>
        </View>
        {/* !!TODO: move the button bellow to the separate button component */}
        <View style={Styles.actionsContainer}>
            <TouchableOpacity style={Styles.deleteButton} onPress={onDelete}>
                <Icon name="delete" size={40} color={AppColors.Black}/>
            </TouchableOpacity>
        </View>
    </ImageCard>);
};

// !!TODO: PropTypes + default values


export default BasketCard;
