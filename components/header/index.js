import React from "react";
import {View, Text} from "react-native"
import Styles from "./styles";
import NavigationDrawerStructure from "../../modules/navigator/navigationDrawerStructure";

const MainHeader = ({children, navigation, title, headerLeft, filters}) => {
    const left = headerLeft ?? <NavigationDrawerStructure navigationProps={navigation}/>
    return (
        <View>
            <View style={Styles.header}>
                <View style={Styles.default}>
                    {left}
                    <Text style={Styles.title}>{title}</Text>
                </View>
                <View style={Styles.children}>
                    {children}
                </View>
            </View>
            <View style={Styles.filters}>
                {filters}
            </View>
        </View>
    );
};

// !!TODO: PropTypes + default values


export default MainHeader;
