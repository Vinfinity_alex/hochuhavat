import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    default: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 3,
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: AppColors.Orange,
        shadowColor: AppColors.Black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 7,
    },
    filters: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: AppColors.VeryLightGrey,
        shadowColor: AppColors.Black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 7,
    },
    filterOpen: {
        padding: 1
    },
    title: {
        paddingLeft:20,
        fontSize: 20,
        fontWeight: "bold",
        color: AppColors.White,
    },
    children: {
        padding:7,
        fontSize: 20,
        fontWeight: "bold",
        color: AppColors.White,
    },
});

export default Styles;