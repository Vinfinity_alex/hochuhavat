import React from "react";
import { TouchableOpacity, View, Text, AsyncStorage } from "react-native";
import AppColors from "../../modules/appColors";
import Icon from "react-native-vector-icons/MaterialIcons";
import Styles from "./styles";

// use prop types https://www.npmjs.com/package/prop-types
// props => {}
const BasketIconButton = ({ style, onPress }, props) => {
  const counter =
    props.itemCount !== "undefined" && props.itemCount > 0 ? (
      <View style={Styles.counter}>
        <Text children={props.itemCount} />
      </View>
    ) : null;

  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <View>
        <Icon name={"shopping-cart"} size={30} style={Styles.icon} />
        {counter}
      </View>
    </TouchableOpacity>
  );
};

// !!TODO: Things to learn
// reselect
// axios
// react-native-extended-stylesheet

// !!TODO: add .editorconfig
// https://editorconfig.org/

// !!TODO: Add eslint to project
// https://www.npmjs.com/package/@react-native-community/eslint-config
// https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb

// !!TODO: use default values for all components
// BasketIconButton.defaultValue = {
//   itemCount: 0
// }
//

// !!TODO: Every component must have propTypes validation
// BasketIconButton.propTypes {
//     onPress: PropType.func.isRequired,
//     style: PropType.obj
// }

export default BasketIconButton;
