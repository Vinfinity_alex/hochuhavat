import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    counter: {
        backgroundColor: AppColors.Red,
        color: AppColors.White,
        borderRadius: 100,
        height: 20,
        width: 20,
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
    },
    icon: {
        margin: 5,
        color: AppColors.White
    }
});

export default Styles;