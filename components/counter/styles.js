import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    countContainer: {
        alignItems: "center",
        justifyContent: "center",
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppColors.Gainsboro
    },
    count: {
        alignItems: "center",
        paddingHorizontal: 5,
        width:80
    },
    countText: {
        fontSize: 30,
        color: AppColors.Black,
        justifyContent: "center",
    },
    btn: {
        alignItems: "center",
        justifyContent: "center",
        width: 60,
    },
});

export default Styles;