import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Styles from "./styles";
import Icon from 'react-native-vector-icons/MaterialIcons';
import AppColors from "../../modules/appColors";


export default class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.count ?? 0
        };
    }

    render() {
        const {value} = this.state;
        const {onPlus, onMinus} = this.props;
        return (
            <View style={Styles.container}>
            {/* make it separate button */}
                <TouchableOpacity style={Styles.btn} onPress={() => {
                    if (value > 0) {
                        this.setState({value: value - 1});
                        onMinus();
                    }
                }}>
                    <Icon name="remove" size={40} color={AppColors.Black}/>
                </TouchableOpacity>

                <View style={Styles.countContainer}>
                    <View style={Styles.count}>
                        <Text style={Styles.countText}>{value}</Text>
                    </View>
                </View>
                {/* !!TODO: make it separate button */}
                <TouchableOpacity style={Styles.btn} onPress={() => {
                    if (value < 9999) {
                        this.setState({value: value + 1});
                        onPlus();
                    }
                }}>
                    <Icon name="add" size={40} color={AppColors.Black}/>
                </TouchableOpacity>
            </View>
        );
    }
}

// !!TODO: PropTypes + default values
