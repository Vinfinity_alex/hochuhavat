import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    container: {
        flex:1,
        overflow: "hidden",
    },
    titleContainer: {
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    titleLeft: {
        minWidth: 30,
        maxWidth: 100,
    },
    titleRight: {
        minWidth: 30,
        maxWidth: 100,
    },
    body: {
        padding: 10,
        paddingTop: 0,
    }
});

export default Styles;