import React, {Component} from "react";
import {Text, Image, TextInput, View, TouchableOpacity, Animated, Easing} from "react-native"
import Styles from "./styles";
import AppColors from "../../modules/appColors";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class ExpandedBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            scaleValue: new Animated.Value(30),
            spinValue: new Animated.Value(0)
        }
    }

    spin(initialValue, finalValue){
        this.state.spinValue.setValue(initialValue);
        Animated.timing(
            this.state.spinValue,
            {
                toValue: finalValue,
                duration: 300,
                easing: Easing.linear,
                useNativeDriver: true
            }
        ).start();
    }

    scale(initialValue, finalValue){
        this.state.scaleValue.setValue(initialValue);
        Animated.spring(
            this.state.scaleValue,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    toggle() {
        let initialScaleValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalScaleValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        let initialSpinValue = this.state.expanded ? 1 : 0,
            finalSpinValue = this.state.expanded ? 0 : 1;

        this.setState({
            expanded: !this.state.expanded
        });

        this.scale(initialScaleValue, finalScaleValue);

        this.spin(initialSpinValue, finalSpinValue);
    }

    render() {
        const spin = this.state.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '180deg']
        })
        return (
            <Animated.View
                style={[Styles.container, {height: this.state.scaleValue}]}>
                <View style={Styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                    <View style={Styles.titleLeft}>
                    {/* !!TODO: Empty text? */}
                        <Text> </Text>
                    </View>
                    {/* !!TODO: What do you need empty View for?  */}
                    <View>
                        <Text children={this.props.title}/>
                    </View>
                    <View style={Styles.titleRight}>
                        <TouchableOpacity onPress={this.toggle.bind(this)}>
                            <Animated.View style={{transform: [{rotate: spin}]}}>
                                <Icon name="keyboard-arrow-down" size={30} color={AppColors.Black}/>
                            </Animated.View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={Styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>
            </Animated.View>
        )
    };
};

// !!TODO: PropTypes + default values
