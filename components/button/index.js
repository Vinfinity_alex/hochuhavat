import React from "react";
import {TouchableOpacity, View} from "react-native"
import Styles from "./styles";

const BaseButton = ({style, onPress, children}) => {
    // !!TODO: Don't do this. style var must be immutable
    // GoodOption const componentStyle = style !== "undefined" ? style : Styles.default;
    // What if style is null? Do you need style or Styles.default?
    style = style !== "undefined" ? style : Styles.default;

    return (
        <TouchableOpacity style={style} onPress={onPress}>
            <View>{children}</View>
        </TouchableOpacity>
    );
};

// !!TODO: PropTypes + default values

export default BaseButton;
