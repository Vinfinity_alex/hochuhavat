import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    default : {
        "alignItems": "center",
        "backgroundColor": AppColors.LightGrey,
        "padding": 10,
    }
});

export default Styles;