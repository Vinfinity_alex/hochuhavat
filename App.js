import React, {Component} from "react";
import AppContainer from "./modules/navigator";
import {Provider} from "react-redux";
import {createStore} from "redux";
import mainReducer from "./modules/reducers/mainReducer";

const store = createStore(mainReducer);

export default class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <AppContainer/>
            </Provider>
        )
    }
}