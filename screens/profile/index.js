import React, {Component} from "react";
import {Text, View, Image} from "react-native";
import Styles from "./styles";

export default class Profile extends Component {
    static navigationOptions = {
        title: "Profile",
    };

    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
                flexDirection: "column",
            }}>
                <View>
                    <Image
                        source={require("../../images/profile-placeholder.png")}
                        style={Styles.avatarIcon}
                    />
                    <Text style={Styles.text}>First Name</Text>
                    <Text style={Styles.text}>Last Name</Text>
                    <Text style={Styles.text}>Phone</Text>
                    <Text style={Styles.text}>Email</Text>
                </View>
            </View>
        );
    }
}