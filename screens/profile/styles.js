import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    avatarIcon: {
        height: 300,
        width: 300,
        margin: 5,
    },

    text: {
        fontSize: 14,
        color: AppColors.Black,
        paddingVertical: 5,
    }
});

export default Styles;