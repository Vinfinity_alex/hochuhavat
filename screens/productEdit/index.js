import React, {Component} from "react";
import {Image, TextInput, View, TouchableOpacity} from "react-native";
import ImagePicker from "react-native-image-picker/index";
import Styles from "./styles";
import BaseButton from "../../components/button";

export default class ProductEdit extends Component {
    constructor(props) {
        super(props);
        const {navigation} = this.props;
        this.state = {
            productID: navigation.getParam("productID"),
            photo: navigation.getParam("photo"),
            productName: navigation.getParam("productName"),
            price: navigation.getParam("price"),
            shortDescription: navigation.getParam("shortDescription"),
            description: navigation.getParam("description"),
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam("title"),
        };
    };

    handleChoosePhoto = () => {
        const options = {
            noData: true,
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                console.log("User cancelled image picker");
            } else if (response.error) {
                console.log("ImagePicker Error: ", response.error);
            } else if (response.customButton) {
                console.log("User tapped custom button: ", response.customButton);
            } else {
                this.setState({
                        photo: response
                    }
                );
            }
        });
    };

    render() {
        const {photo} = this.state;
        const image = <Image
            source={photo != null ? {uri: photo.uri} : require("../../images/profile-placeholder.png")}
            style={Styles.avatarIcon}
        />

        return (
            <View style={{flex: 1, alignItems: 'stretch'}}>
                <BaseButton onPress={this.handleChoosePhoto} style={{alignItems: "center", position: "relative"}}
                            children={image}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.productName = {text})}
                    value={this.state.productName}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.price = {text})}
                    value={this.state.price}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.shortDescription = {text})}
                    value={this.state.shortDescription}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.description = {text})}
                    value={this.state.description}/>
            </View>
        );
    }
}