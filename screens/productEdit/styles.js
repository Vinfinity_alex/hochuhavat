import React from "react";
import {StyleSheet} from "react-native";

const Styles = StyleSheet.create({
    avatarIcon: {
        height: 300,
        width: 300,
        margin: 5,
    },
    textInput: {
        height: 40,
        width: 300,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        margin: 5,
        alignSelf: "center"
    }
});

export default Styles;