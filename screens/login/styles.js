import React from "react";
import {Dimensions, StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Window = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        borderColor: AppColors.LightGrey,
        borderBottomWidth:1,
        width: Window.width - 100,
    }
});

export default Styles;