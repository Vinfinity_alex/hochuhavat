import React, {Component} from "react";
import {View, TextInput, Animated} from "react-native";
import Styles from "./styles";

export default class Login extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            header: null,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            login: "",
            password: ""
        };
    }

    _login = () => {
        if (this.state.login === "admin" && this.state.password === "admin")
            this.props.navigation.replace("Admin");
        if (this.state.login === "guest" && this.state.password === "guest")
            this.props.navigation.replace("App");
    }

    render() {
        this._login();
        return (
            <View style={Styles.container}>
                <TextInput style={Styles.textInput} onChangeText={(text) => {
                    this.setState({login: text ?? "", password:this.state.password});
                }} placeholder={"Login or email"}/>
                <TextInput style={Styles.textInput} onChangeText={(text) => {
                    this.setState({login: this.state.login, password:text ?? ""});
                }} placeholder={"Password"}/>
            </View>
        );
    }
}