import React, {Component} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {addProduct} from "../basket/actions";
import {Image, Text, View, TouchableOpacity, ScrollView, AsyncStorage} from "react-native";

import Styles from "./styles";

import ImageSlider from 'react-native-image-slider';
import ProductDescription from "../../components/productDescription";
import BaseButton from "../../components/button";
import BasketCard from "../../components/basketCard";


class ProductDetails extends Component {
    constructor(props) {
        super(props);
        const {navigation} = this.props;
        this.state = {
            productID: navigation.getParam('productID'),
            photos: navigation.getParam('photos'),
            productName: navigation.getParam('productName'),
            price: navigation.getParam('price'),
            shortDescription: navigation.getParam('shortDescription'),
            description: navigation.getParam('description'),
            amount: 1
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('title'),
        };
    };

    addToBasket = () => {
        this.props.addProduct(this.state)
    };

    render() {
        const {photos} = this.state;
        return (
            <View style={{flex: 1}}>
                <ImageSlider
                    style={Styles.imageSlider}
                    loopBothSides
                    images={photos}
                    customSlide={({index, item, style}) => (
                        <View key={index} style={[style]}>
                            <Image source={{uri: item}} style={Styles.avatarIcon}/>
                        </View>
                    )}
                />
                <ProductDescription price={this.state.price} shortDescription={this.state.shortDescription}
                                    description={this.state.description}/>
                <BaseButton style={Styles.button} onPress={this.addToBasket}
                            children={<Text style={Styles.buttonText} children={"ORDER"}/>}/>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {basket} = state;
    return {basket}
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        addProduct,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);