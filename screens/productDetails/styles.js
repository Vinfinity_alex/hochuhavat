import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    description: {
        flex: 1,
    },
    avatarIcon: {
        height: 200,
    },
    imageSlider: {
        height: 200,
        flex: 0,
    },
    button:{
        backgroundColor: AppColors.Violet,
        height:70,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText:{
        fontSize: 18,
        fontWeight: "bold",
        color: AppColors.White,
    }
});

export default Styles;