import React, {Component} from "react";
import {Image, TextInput, View} from "react-native";
import ImagePicker from "react-native-image-picker/index";
import Styles from "./styles";
import BaseButton from "../../components/button";

export default class ProfileEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: null,
            firstNameTxt: "Useless Placeholder",
            lastNameTxt: "Last Name",
            emailTxt: "Email",
            phoneTxt: "Phone",
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam("otherParam", "A Nested Details Screen"),
        };
    };

    handleChoosePhoto = () => {
        const options = {
            noData: true,
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                console.log("User cancelled image picker");
            } else if (response.error) {
                console.log("ImagePicker Error: ", response.error);
            } else if (response.customButton) {
                console.log("User tapped custom button: ", response.customButton);
            } else {
                this.setState({
                        photo: response
                    }
                );
            }
        });
    };

    render() {
        const {photo} = this.state;

        let image = <Image
            source={photo != null ? {uri: photo.uri} : require("../../images/profile-placeholder.png")}
            style={Styles.avatarIcon}
        />

        return (
            <View style={{flex: 1, alignItems: 'stretch'}}>
                <BaseButton onPress={this.handleChoosePhoto} style={{alignItems: "center", position: "relative"}}
                            children={image}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.firstNameTxt = {text})}
                    value={this.state.firstNameTxt}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.lastNameTxt = {text})}
                    value={this.state.lastNameTxt}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.emailTxt = {text})}
                    value={this.state.emailTxt}/>
                <TextInput
                    style={Styles.textInput}
                    onChangeText={(text) => this.setState(this.state.phoneTxt = {text})}
                    value={this.state.phoneTxt}/>
            </View>
        );
    }
}