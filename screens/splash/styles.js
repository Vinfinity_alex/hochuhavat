import React from "react";
import {Dimensions, StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Window = Dimensions.get('window');

const Styles = StyleSheet.create({
    logo: {
        backgroundColor: AppColors.Orange,
        justifyContent: 'center',
        alignItems: 'center',
        height: Window.height,
        width: Window.width,
    },
});

export default Styles;