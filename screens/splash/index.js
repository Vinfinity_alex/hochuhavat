import React, {Component} from "react";
import {View, Text, Image, Animated, TouchableOpacity} from "react-native";
import Styles from "./styles";

export default class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fadeAnim: new Animated.Value(0),
        };
    }

    componentDidMount() {
        Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 1,
                duration: 3000,
            }
        ).start(()=>{this.props.navigation.replace("Login")});
    }

    render() {
        let {fadeAnim} = this.state;

        return (
            <TouchableOpacity onPress={() => {
                this.props.navigation.replace("Login")
            }}>
                <Animated.View style={{opacity: fadeAnim}}>
                    <Image style={Styles.logo} resizeMode="contain" source={require("../../images/logo2.png")}/>
                </Animated.View>
            </TouchableOpacity>
        );
    }
}