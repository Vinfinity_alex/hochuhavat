export const loadProducts = productList => (
    {
        type: "LOAD_PRODUCTS",
        payload: productList,
    }
);

export const filterProduct = filter => (
    {
        type: "FILTER_PRODUCTS",
        payload: filter,
    }
);

export const reloadCounterUp = () => (
    {
        type: "RELOAD_COUNTER_UP",
        payload: {},
    }
);

export const reloadCounterReset = () => (
    {
        type: "RELOAD_COUNTER_RESET",
        payload: 0,
    }
);