import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    catalogContainer: {
        flex: 1,
        alignItems: "stretch",
        justifyContent: "center",
    },
    activity: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    reload: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    reloadButton: {
        alignSelf: "center",
        justifyContent: "center",
    }
});

export default Styles;