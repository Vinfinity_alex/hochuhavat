import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {Text, View, FlatList, ActivityIndicator} from "react-native";
import {loadProducts, filterProduct, reloadCounterUp, reloadCounterReset} from "./actions";

import Styles from "./styles";

import AppColors from "../../modules/appColors";
import {getProducts, isConnected} from "../../api/client";
import Icon from "react-native-vector-icons/MaterialIcons";
import BaseButton from "../../components/button";
import CatalogCard from "../../components/catalogCard";
import BasketCard from "../../components/basketCard";

class Catalog extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('otherParam', 'Catalog'),
        };
    };

    _loadProducts = async () => {
        const products = await getProducts();
        console.log(products);
        if (typeof products == "string") {
            setTimeout(function (loadProducts, reloadCounterUp, counter) {
                if (counter <= 5) {
                    loadProducts();
                    reloadCounterUp();
                }
                console.log("Reload " + counter);
            }, 200, this._loadProducts, this.props.reloadCounterUp, this.props.catalog.reloadCounter);
        } else {
            this.props.reloadCounterReset();
            this.props.loadProducts(products);
        }
    };

    _keyExtractor = (item, index) => `product ${index}`;

    _renderItem = (({item}) => <CatalogCard
            productID={item.productID}
            navigation={this.props.navigation}
            imagesUri={item.images}
            title={item.productName}
            price={item.price}
            shortDescription={item.productShortDescript}
            description={item.productDescript}
            onLongPress={() => {
            }}
        />
    );

    render() {
        let catalog;
        const {products, reloadCounter} = this.props.catalog;
        if (reloadCounter <= 5) {
            if (typeof products !== "undefined" && products.length > 0) {
                catalog = <FlatList
                    data={products}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                />;
            } else {
                catalog = <ActivityIndicator style={Styles.activity} size={100} color={AppColors.Darkviolet}/>;
                this._loadProducts();
            }
        } else {
            catalog =
                <BaseButton style={Styles.reload} onPress={() => {
                    this.props.reloadCounterReset();
                    this.props.loadProducts(products);
                }}>
                    <Icon style={Styles.reloadButton} name="refresh" size={100} color={AppColors.Salmon}/>
                    <Text>Press on screen to reload</Text>
                </BaseButton>;
        }

        return (
            <View style={Styles.catalogContainer}>
                {catalog}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {catalog} = state;
    return {catalog}
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        loadProducts,
        filterProduct,
        reloadCounterUp,
        reloadCounterReset,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);