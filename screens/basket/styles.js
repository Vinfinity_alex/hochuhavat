import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../../modules/appColors";

const Styles = StyleSheet.create({
    catalogContainer: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    button:{
        backgroundColor: AppColors.Violet,
        height:70,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText:{
        fontSize: 18,
        fontWeight: "bold",
        color: AppColors.White,
    },
    emptyCatalogContainer: {
        fontSize: 20,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});

export default Styles;