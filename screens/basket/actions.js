export const addProduct = product => (
    {
        type: 'ADD_PRODUCT',
        payload: product,
    }
);

export const reduceProduct = product => (
    {
        type: 'REDUCE_PRODUCT',
        payload: product,
    }
);

export const deleteProduct = productID => (
    {
        type: 'DELETE_PRODUCT',
        payload: productID,
    }
);

export const clearBasket = () => (
    {
        type: 'CLEAR_BASKET',
        payload: [],
    }
);