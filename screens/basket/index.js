import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {deleteProduct, addProduct, reduceProduct, clearBasket} from "./actions";

import Styles from "./styles";

import {FlatList, Text, View} from "react-native";
import BasketCard from "../../components/basketCard";
import BaseButton from "../../components/button";
import BasketHeader from "../../components/basketHeader";

class Basket extends Component {
    static navigationOptions = ({navigation, basket}) => ({
        header: () => <BasketHeader navigation={navigation}/>
    });

    _keyExtractor = (item, index) => `product ${index}`;

    _renderItem = (({item}) => <BasketCard
            keyExtractor={(item: object, index: number) => item.productID}
            productID={item.productID}
            navigation={this.props.navigation}
            imagesUri={item.photos}
            title={item.productName}
            price={item.price}
            amount={item.amount}
            onDelete={() => {
                this.props.deleteProduct(item.productID)
            }}
            onPlus={() => {
                this.props.addProduct(item)
            }}
            onMinus={() => {
                this.props.reduceProduct(item)
            }}
        />
    );

    render() {
        const {basket:{productList}, navigation} = this.props;
        const catalog = (typeof productList !== "undefined" && productList.length > 0) ?
            <View style={{flex: 1}}>
                <FlatList
                    data={productList}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                />
                <BaseButton style={Styles.button} onPress={() => {
                    navigation.replace("Catalog");
                    this.props.clearBasket();
                }}>
                    <Text style={Styles.buttonText} children={"ORDER"}/>
                </BaseButton>
            </View>
            :
            <Text style={Styles.emptyCatalogContainer}>Basket is empty</Text>;

        return (
            <View style={Styles.catalogContainer}>
                {catalog}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {basket, catalog} = state;
    return {basket, catalog}
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        deleteProduct,
        addProduct,
        reduceProduct,
        clearBasket
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Basket);