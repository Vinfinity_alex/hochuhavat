import React from "react";
import {StyleSheet} from "react-native";

const Styles = StyleSheet.create({
    viewPager: {
        flex: 1
    },
    pageStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    }
});

export default Styles;