import React, {Component} from "react";
import {Button, Text, View, ViewPagerAndroid} from "react-native";
import Styles from "./styles";

export default class Settings extends Component{
    static navigationOptions = {
        title: "Settings",
    };

    render() {
        return (
            <ViewPagerAndroid
                style={Styles.viewPager}
                initialPage={0}>
                <View style={Styles.pageStyle} key="1">
                    <Text>First page</Text>
                </View>
                <View style={Styles.pageStyle} key="2">
                    <Text>Second page</Text>
                </View>
                <View style={Styles.pageStyle}>
                    <Text>Settings Screen</Text>

                    <Button
                        title="Go back"
                        onPress={() => this.props.navigation.goBack()}
                    />
                </View>
            </ViewPagerAndroid>

        );
    }
}