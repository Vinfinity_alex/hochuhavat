export default class AppColors {
    static Black = "#000";
    static White = "#fff";


    static Yellow = "#ffff00";
    static Salmon = "#fa8072";
    static Orange = "#ffa500";
    static Red = "#FF0000";

    static VeryLightGrey = "#E3E3E3";
    static Gainsboro = "#dcdcdc";
    static LightGrey = "#DDDDDD";
    static Honeydew = "#f0fff0";
    static Powderblue = "#b0e0e6";
    static Lightsteelblue = "#b0c4de";
    static Grey = "#808080";

    static Deepskyblue = "#00bfff";
    static Cyan = "#00ffff";
    static Blue = "#0000ff";

    static Violet = "#ee82ee";
    static Darkviolet = "#9400d3";

    static Palegreen = "#98fb98";
    static Springgreen = "#00ff7f";
    static Lawngreen = "#7cfc00";
    static Lime = "#00ff00";
}