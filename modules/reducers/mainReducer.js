import {combineReducers} from 'redux';
import {
    ADD_PRODUCT,
    CLEAR_BASKET,
    DELETE_PRODUCT,
    LOAD_PRODUCTS,
    FILTER_PRODUCTS,
    REDUCE_PRODUCT,
    RELOAD_COUNTER_UP,
    RELOAD_COUNTER_RESET
} from "../types/types";

const INITIAL_BASKET_STATE = {
    productList: [],
    fullPrice: 0
};

const INITIAL_CATALOG_STATE = {
    products: [],
    reloadCounter: 0,
};

const basketReducer = (state = INITIAL_BASKET_STATE, action) => {

    const copyWith = props => {
        const {
            productList,
            fullPrice
        } = state;

        const products = props.productList ?? productList;
        const price = props.fullPrice ?? fullPrice;

        return {productList: products, fullPrice: price}
    };

    switch (action.type) {
        case ADD_PRODUCT: {
            const {
                productList,
            } = state;
            let newProductList = productList ?? [];
            const current = newProductList.find(x => x.productID === action.payload.productID);
            if (current !== undefined)
                current.amount = current.amount + 1;
            else
                newProductList.push(action.payload);

            const newState = copyWith({productList: newProductList});
            return newState;
        }

        case REDUCE_PRODUCT: {
            const {
                productList,
            } = state;
            let newProductList = productList ?? [];
            const current = newProductList.find(x => x.productID === action.payload.productID);
            if (current !== undefined)
                if (current.amount > 1)
                    current.amount = current.amount - 1;
                else
                    newProductList = newProductList.filter(x => x.productID !== action.payload.productID);

            const newState = copyWith({productList: newProductList});
            return newState;
        }

        case DELETE_PRODUCT: {
            const {
                productList,
            } = state;

            const newProductList = productList.filter(x => x.productID !== action.payload);

            const newState = copyWith({productList: newProductList});
            return newState;
        }

        case CLEAR_BASKET: {
            return copyWith({productList: action.payload});
        }

        default:
            return state
    }
};

const catalogReducer = (state = INITIAL_CATALOG_STATE, action) => {

    const copyWith = props => {
        const {
            products,
            reloadCounter
        } = state;

        const productList = props.products ?? products;
        const counter = props.reloadCounter ?? reloadCounter;

        return {products: productList, reloadCounter: counter}
    };

    switch (action.type) {
        case LOAD_PRODUCTS: {
            const newState = copyWith({products: action.payload});
            return newState;
        }

        case FILTER_PRODUCTS: {
            const {
                products
            } = state;

            const pureArray = products.filter(e => e === action.payload);
            products.splice(0);
            products.push(...pureArray);

            const newState = {products: pureArray};
            return newState;
        }

        case RELOAD_COUNTER_UP: {
            const {products, reloadCounter} = state;
            let newReloadCounter = reloadCounter ?? 0;
            newReloadCounter = newReloadCounter + 1;

            const newState = {products: products, reloadCounter: newReloadCounter};
            return newState;
        }

        case RELOAD_COUNTER_RESET: {
            const {products} = state;

            const newState = {products: products, reloadCounter: action.payload};
            return newState;
        }

        default:
            return state;
    }
};

export default combineReducers({
    basket: basketReducer,
    catalog: catalogReducer,
});