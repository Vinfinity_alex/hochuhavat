import React, {Component} from 'react';

import {View, Text, TouchableOpacity, Button} from 'react-native';

import {
    createAppContainer,
    createStackNavigator,
    createDrawerNavigator,
} from 'react-navigation';

import Icon from "react-native-vector-icons/MaterialIcons";

import Profile from "../../screens/profile";
import Settings from "../../screens/settings";
import ProfileEdit from "../../screens/profileEdit";
import Catalog from "../../screens/catalog";
import Basket from "../../screens/basket";
import Splash from "../../screens/splash";
import ProductDetails from "../../screens/productDetails";
import ProductEdit from "../../screens/productEdit";

import BasketIcon from "../../components/basketIconButton";
import MainHeader from "../../components/header";
import NavigationDrawerStructure from "./navigationDrawerStructure";
import BackButton from "../../components/backButton";
import ExpandedBar from "../../components/expandedBar";
import SearchBar from "../../components/searchBar";
import Styles from "./styles";
import AppColors from "../appColors";
import Login from "../../screens/login";

const HomeStack = createStackNavigator({
    Catalog: {
        screen: Catalog,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader title={"Home"}
                                navigation={navigation}
                                filters={<ExpandedBar><SearchBar onPress={()=>{}}/></ExpandedBar>}>
                <BasketIcon itemCount={12} onPress={() => {
                    navigation.push("Basket")
                }}/>
            </MainHeader>,
        }),
    },
    Basket: {
        screen: Basket,
    },
    ProductDetails: {
        screen: ProductDetails,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader title={navigation.getParam('title')} navigation={navigation} headerLeft={<BackButton navigation={navigation}/>}>
                <BasketIcon itemCount={12} onPress={() => {
                    navigation.push("Basket")
                }}/>
            </MainHeader>,
        }),
    },
    ProductEdit: {
        screen: ProductEdit,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader title={"Home"} navigation={navigation}>
                <View style={Styles.textButton}>
                    <TouchableOpacity onPress={() => {
                        navigation.push("ProductDetails");
                    }}>
                        <Icon name="edit" size={30} color={AppColors.White}/>
                    </TouchableOpacity>
                </View>
            </MainHeader>,
        }),
    }
});

const ProfileStack = createStackNavigator({
    Profile: {
        screen: Profile,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader title={"Profile"} navigation={navigation}>
                <TouchableOpacity onPress={() => {
                    navigation.push("ProfileEdit");
                }}>
                    <Icon name="edit" size={30} color={AppColors.White}/>
                </TouchableOpacity>
            </MainHeader>,
        }),
    },
    ProfileEdit: {
        screen: ProfileEdit,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader title={"Edit profile"} headerLeft={<BackButton navigation={navigation}/>}
                                navigation={navigation}>
                <View style={Styles.textButton}>
                    <Button title={"Save"} onPress={() => navigation.pop()}/>
                </View>
            </MainHeader>
        }),
    }
});

const SettingsStack = createStackNavigator({
    Settings: {
        screen: Settings,
        navigationOptions: ({navigation}) => ({
            header: <MainHeader
                title={"Settings"}
                headerLeft={<NavigationDrawerStructure navigationProps={navigation}/>}
                navigation={navigation}>
            </MainHeader>,
        }),
    },
});

const DrawerNavigator = createDrawerNavigator({
        Home: {
            screen: HomeStack,
            navigationOptions: {
                drawerLabel: "Home",
            },
        },
        Profile: {
            screen: ProfileStack,
            navigationOptions: {
                drawerLabel: "Profile",
            },
        },
        Settings: {
            screen: SettingsStack,
            navigationOptions: {
                drawerLabel: "Settings",
            },
        }
    },
    {initialRouteName: "Home"}
);

const AppDrawerNavigator = createAppContainer(DrawerNavigator);

const AppContainer = createStackNavigator({
        Splash: {
            screen: Splash,
            navigationOptions: {
                header: null,
            },
        },
        Login: {
            screen: Login
        },
        App: {
            screen: AppDrawerNavigator,
            navigationOptions: {
                header: null,
            },
        },
        Admin: {
            screen: AppDrawerNavigator,
            navigationOptions: {
                header: null,
            },
        },
    },
    {initialRouteName: "Splash"});

const App = createAppContainer(AppContainer);

export default App;