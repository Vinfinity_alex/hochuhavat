import React, {Component} from "react";
import {TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import AppColors from "../appColors";

export default class NavigationDrawerStructure extends Component {
    toggleDrawer = () => {
        this.props.navigationProps.toggleDrawer();
    };

    render() {
        return (
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
                    <Icon name="menu" size={30} color={AppColors.White} style={{padding: 10}}/>
                </TouchableOpacity>
            </View>
        );
    }
}
