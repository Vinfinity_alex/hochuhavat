import React from "react";
import {StyleSheet} from "react-native";
import AppColors from "../appColors";

const Styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: "bold",
        color: AppColors.Black,
    },

    default: {
        fontSize: 14,
        color: AppColors.Black,
        paddingVertical: 5,
    },

    price: {
        fontSize: 16,
        color: AppColors.Black,
        paddingVertical: 5,
        fontStyle: "italic"
    }
});

export default Styles;